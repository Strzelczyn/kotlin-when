fun main(args: Array<String>) {
    var valueReaded: Any = readLine()!!
    when (valueReaded) {
        is Int -> {
            print("value is type int")
        }
        1, 2, 3 -> {
            print("value is 1 or 2 or 3")
        }
        in 4..10 -> {
            print("value from 4 to 10")
        }
        else -> {
            print("value isn't from 1 to 10")
        }
    }
}